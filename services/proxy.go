package services

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"proxy_cal_server/models"
)

type ProxyService struct {
}

func (p *ProxyService) NewProxyHandler() (*models.CalProxy, error) {
	targetURL := os.Getenv("CALCULATOR_SERVER_URL")
	urlParsed, err := url.Parse(targetURL)

	if err != nil {
		return nil, err
	}

	singHostProxy := httputil.NewSingleHostReverseProxy(urlParsed)
	singHostProxy.Transport = &ProxyService{}

	return &models.CalProxy{
		Target: urlParsed,
		Proxy:  singHostProxy,
	}, nil
}

func (p *ProxyService) RoundTrip(request *http.Request) (*http.Response, error) {

	buf, _ := ioutil.ReadAll(request.Body)
	request.Body = ioutil.NopCloser(bytes.NewBuffer(buf))
	// rdr2 := ioutil.NopCloser(bytes.NewBuffer(buf))

	response, err := http.DefaultTransport.RoundTrip(request)
	if err != nil {
		log.Println("came in error resp here", err)
		return nil, err // server is not reachable. server not working
	}

	body, err := httputil.DumpResponse(response, true)
	if err != nil {
		log.Println("error in dumb response")
		// copying the response body did not work
		return nil, err
	}

	log.Println("response Body : ", string(body))
	return response, err
}
