package services

import (
	"proxy_cal_server/models"
)

type IProxyService interface {
	NewProxyHandler() (*models.CalProxy, error)
}
