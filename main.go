package main

import (
	"log"
	"net/http"
	"os"
	"proxy_cal_server/controllers"
	"proxy_cal_server/routes"
	"proxy_cal_server/services"
	"time"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("error loading .env file")
	}

	cont := &controllers.ControllerStruct{
		ProxyService: &services.ProxyService{},
	}

	route := routes.Route{Controller: cont}
	srv := &http.Server{
		Handler: route.Routes(),
		Addr:    os.Getenv("PROXY_CALCULATOR_PORT"),
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
