package models

import (
	"net/http/httputil"
	"net/url"
)

type CalProxy struct {
	Target *url.URL
	Proxy  *httputil.ReverseProxy
}
