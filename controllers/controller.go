package controllers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"proxy_cal_server/models"
	"proxy_cal_server/services"
)

type ControllerStruct struct {
	ProxyService services.IProxyService
}

func (c *ControllerStruct) Sum(w http.ResponseWriter, r *http.Request) {
	proxy, err := c.ProxyService.NewProxyHandler()
	if err != nil {
		c.failCase(w, err)
		return
	}

	proxy.Proxy.ServeHTTP(w, r)
	return
}

func (c *ControllerStruct) Mul(w http.ResponseWriter, r *http.Request) {
	proxy, err := c.ProxyService.NewProxyHandler()
	if err != nil {
		c.failCase(w, err)
		return
	}

	proxy.Proxy.ServeHTTP(w, r)
	return
}

func (c *ControllerStruct) Sub(w http.ResponseWriter, r *http.Request) {
	proxy, err := c.ProxyService.NewProxyHandler()
	if err != nil {
		c.failCase(w, err)
		return
	}

	proxy.Proxy.ServeHTTP(w, r)
	return
}

func (c *ControllerStruct) Div(w http.ResponseWriter, r *http.Request) {
	proxy, err := c.ProxyService.NewProxyHandler()
	if err != nil {
		c.failCase(w, err)
		return
	}

	proxy.Proxy.ServeHTTP(w, r)
	return
}

func (c *ControllerStruct) failCase(w http.ResponseWriter, err error) {
	data := &models.FailCase{
		StatusText:   http.StatusText(http.StatusBadRequest),
		ErrorMessage: err.Error(),
	}
	strData, err := json.Marshal(data)
	if err != nil {
		log.Fatal("unmarshal data fail")
	}
	w.Header().Add("content-type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	_, err = w.Write(strData)
	if err != nil {
		log.Fatal("writing byte fail")
	}
}

func (c *ControllerStruct) successCase(w http.ResponseWriter, body interface{}) {
	data := &models.SuccessCase{
		StatusText: http.StatusText(http.StatusOK),
		Data:       body,
	}
	strData, err := json.Marshal(data)
	if err != nil {
		log.Fatal("unmarshal data fail")
	}
	w.Header().Add("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(strData)
	if err != nil {
		log.Fatalf("writing byte fail")
	}
}

func (c *ControllerStruct) readBody(r *http.Request) *models.RequestBody {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal("fail to read request body")
	}
	objBody := new(models.RequestBody)
	err = json.Unmarshal(body, objBody)
	if err != nil {
		log.Fatal("fail while unmarshal request body")
	}

	return objBody
}
