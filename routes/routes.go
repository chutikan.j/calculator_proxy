package routes

import (
	"net/http"
	"proxy_cal_server/controllers"

	"github.com/gorilla/mux"
)

type Route struct {
	Controller controllers.IController
}

func (r *Route) Routes() *mux.Router {
	route := mux.NewRouter()

	route.HandleFunc("/calculator.sum", r.Controller.Sum).Methods(http.MethodPost).Name("sum")
	route.HandleFunc("/calculator.mul", r.Controller.Mul).Methods(http.MethodPost).Name("mul")
	route.HandleFunc("/calculator.sub", r.Controller.Sub).Methods(http.MethodPost).Name("sub")
	route.HandleFunc("/calculator.div", r.Controller.Div).Methods(http.MethodPost).Name("div")

	r.AddProxyHeaderMiddleware(route)
	return route
}

func (r *Route) AddProxyHeaderMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("X-GoProxy", "CalculatorProxy")
		h.ServeHTTP(w, req)
	})
}
